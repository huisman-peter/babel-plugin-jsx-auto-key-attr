"use strict";

module.exports = function (babel) {

  var t = babel.types;

  var keyValue = 0;

  var getKeyValue = function () {
    keyValue++;
    return "" + keyValue;
  };

  return {
    visitor: {
      JSXElement: function transform(path) {
        let { node } = path;
        let keyAttr = node.openingElement.attributes
          .filter(({ type, name }) => type === 'JSXAttribute' && name.name === 'key');
        if (!keyAttr.length) {
          const k = t.jSXIdentifier("key");
          const v = t.StringLiteral(getKeyValue());
          const attr = t.jSXAttribute(k, v);
          node.openingElement.attributes.push(attr);
        }
      }
    }
  };

};